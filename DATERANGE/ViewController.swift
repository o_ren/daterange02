

import UIKit
import AVKit

class ViewController: UIViewController {
	
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	@IBAction func play(_ sender: UIButton) {
		
		
		let object = PlaybackController()
		object.prepareToPlay(fromController : self)
	}
}


	let urls = ["https://mako-hls-vod.akamaized.net/VOD/Oren/fmp4/mp4-03/index.m3u8",
	"https://cdn.theoplayer.com/video/star_wars_episode_vii-the_force_awakens_official_comic-con_2015_reel_(2015)/index-daterange.m3u8",
	"https://keshethlslive-i.akamaihd.net/hls/live/594485/TEST02/index.m3u8"]

class PlaybackController: NSObject, AVPlayerItemMetadataCollectorPushDelegate {
	
	let player = AVPlayer()
	var playerItem: AVPlayerItem!
	let controller = AVPlayerViewController()
	var metadataCollector: AVPlayerItemMetadataCollector!
	let videoURL = URL(string: urls[1])
	var timer = Timer()
	
	
	
	func prepareToPlay(fromController : UIViewController) {
		
		let TAG:String! = AVMetadataItem.identifier(forKey: "X-MAKO-URL", keySpace: .hlsDateRange)!.rawValue
		metadataCollector = AVPlayerItemMetadataCollector(identifiers: [TAG], classifyingLabels: nil)
		metadataCollector.setDelegate(self, queue: .main)
		
		playerItem = AVPlayerItem(url: videoURL!)
		playerItem.add(metadataCollector)
	
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)

		let layer: AVPlayerLayer = AVPlayerLayer(player: player)
		
		layer.frame = fromController.view.bounds
		
		player.replaceCurrentItem(with: playerItem)
		//controller.showsPlaybackControls = true
		//controller.player = player
		
		DispatchQueue.main.async {
			self.player.play()
			fromController.view.layer.addSublayer(layer)
		}
		
		//fromController.present(controller, animated: true)
		timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
	}
	
	@objc func playerDidFinishPlaying(sender: Notification) {
		player.pause()
		print("TEST: finished")
	}
	
	
	@objc func updateCounting(){
//		print("WTF :: \(playerItem.externalMetadata)")
		
		// Mark: - read #EXT-DATE-TIME tag from m3u8
		let hlsTime = playerItem.currentTime()
		let hlsDate = playerItem.currentDate()
		
		var hmsTime:String {
			let totalSeconds = CMTimeGetSeconds(hlsTime)
			let hours:Int = Int(totalSeconds / 3600)
			let minutes:Int = Int(totalSeconds.truncatingRemainder(dividingBy: 3600) / 60)
			let seconds:Int = Int(totalSeconds.truncatingRemainder(dividingBy: 60))

			if hours > 0 {
				return String(format: "%i:%02i:%02i", hours, minutes, seconds)
			} else {
				return String(format: "%02i:%02i", minutes, seconds)
			}
		}
//		print ("HLS TIME IS \(hmsTime)")
//		print ("HLS DATE IS \(hlsDate!)")
		
	}
	
	
	func metadataCollector(_ metadataCollector: AVPlayerItemMetadataCollector, didCollect metadataGroups: [AVDateRangeMetadataGroup], indexesOfNewGroups: IndexSet, indexesOfModifiedGroups: IndexSet) {
		
		print("METADATAGROUP: \(metadataGroups)")
	}
}
